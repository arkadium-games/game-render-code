/**
 * Created by ilja on 12/8/2015.
 */

var DeviceUtils = require("DeviceUtils");

var GamePreference = function (){};

GamePreference.prototype = {
    meta:'',
    render:'',
    optionalParams:{},
    height:undefined,
    width:undefined
};

GamePreference.prototype.setDefaultParams = function (initialScale) {
    "use strict";
    this.render = 'iframe';
    this.setDefaultMeta(initialScale);
};

GamePreference.prototype.setDefaultMeta = function (initialScale) {
    "use strict";
    this.meta = 'target-densitydpi=device-dpi, initial-scale=' + initialScale + ', user-scalable=no';
};

/*
* Tries to load external file with additional preferences for the game
* @param {object} jquery - jquery instance
 * @param {string} url - url string to load the file from
 * @param {object} optionalParams - object with optional params
 * @param {function} callback - calls, when loading and parsing process done
 */
GamePreference.prototype.loadExternalPreference = function (jquery, url, optionalParams, callback) {
    "use strict";
    this._externalPreferenceLoadedCallback = callback;
    jquery.ajax({
        type:'GET',
        dataType:'json',
        url:url,
        success: (function (content) {
            this.parseLoadedPreference(content, optionalParams, jquery);
            this._externalPreferenceLoadedCallback();
        }).bind(this),
        error: (function ( jqXHR, textStatus, errorThrown) {
            this.setDefaultParams(DeviceUtils.findScale());
            this._externalPreferenceLoadedCallback();
        }).bind(this)
    });
};

GamePreference.prototype.parseLoadedPreference = function (content, optionalParams, jquery) {
    "use strict";
    for (var obj in content) {
        this[obj] = content[obj];
    }

    if(this.meta == undefined && jquery('head > meta[name="viewport"]').length == 0){
        this.setDefaultMeta(DeviceUtils.findScale());
    }

    if(content.optionalParams != undefined){
        for (var obj in content.optionalParams) {
            if(optionalParams[obj] == undefined){
                optionalParams[obj] = content.optionalParams[obj];
            }
        }
    }
};

module.exports = GamePreference;
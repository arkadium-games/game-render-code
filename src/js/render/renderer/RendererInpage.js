/**
 * Created by ilja on 12/23/2015.
 */

var ParamsUtils = require("ParamsUtils");

var RendererInpage = function () {
    "use strict";

};

RendererInpage.prototype.render = function (jquery, optionalParams, url, idDiv) {
    "use strict";

    this._jquery = jquery;
    this._optionalParams = optionalParams;
    this._url = url;
    this._idDiv = idDiv;
    this._gameProtocolParams = ParamsUtils.checkGameProtocolParams(this._optionalParams);
    this._jquery.ajax({
        type: 'GET',
        url: this._url + '/game-init.html?' + Math.random(),
        success: (this.onGameInitHtmlLoaded).bind(this)
    });
};

RendererInpage.prototype.onGameInitHtmlLoaded = function (file_html) {
    "use strict";

    var regExp = new RegExp("{GameUrl}", "g");
    file_html = file_html.replace(regExp,(this._url + '/'));
    this._jquery("#" + this._idDiv).html(file_html);
    this.runGame(this);
};

RendererInpage.prototype.runGame = function (context) {
    "use strict";

    if(typeof ARK_game_startGame == 'function'){
        ARK_game_startGame({assetPath:(context._url + '/'),gameParams:context._gameProtocolParams.replace('?','')});
    } else {
        var fun = function(){
            context.runGame(context);
        }
        setTimeout(fun, 100);
    }
};

module.exports = RendererInpage;
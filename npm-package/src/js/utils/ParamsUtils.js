/**
 * Created by ilja on 12/8/2015.
 */

var ParamsUtils = {

    /*
    * Parses optional params and compose an object
     * @param {object} [optionalParams]
    * @return {object}
     */
    checkGameProtocolParams:function (optionalParams){
        "use strict";
        var gameProtocolParams = '';
        if (optionalParams && typeof optionalParams === 'object') {
            if (optionalParams.gameProtocolParams && typeof optionalParams.gameProtocolParams === 'string' && optionalParams.gameProtocolParams.length > 0) {
                gameProtocolParams = '?' + optionalParams.gameProtocolParams;
                if (optionalParams.appendAdditionalParamsToUrl) {
                    for (var key in optionalParams.appendAdditionalParamsToUrl) {
                        gameProtocolParams = gameProtocolParams+'&'+key+'='+optionalParams.appendAdditionalParamsToUrl[key];
                    }
                }
            }
            else{
                gameProtocolParams = '?';
                if (optionalParams.appendAdditionalParamsToUrl) {
                    for (var key in optionalParams.appendAdditionalParamsToUrl) {
                        gameProtocolParams = gameProtocolParams+'&'+key+'='+optionalParams.appendAdditionalParamsToUrl[key];
                    }
                }
            }
        }
        return gameProtocolParams;
    },

    /*
    * Parses optional params and finds out if the force render type is set
    * @param {object} optionalParams
     * @return {string}
     */
    checkForceRenderType: function(optionalParams){
        "use strict";
        if (optionalParams && typeof optionalParams === 'object') {
            if (optionalParams.forceRenderType) {
                return optionalParams.forceRenderType;
            }
        }

        return null;
    }

};

module.exports = ParamsUtils;
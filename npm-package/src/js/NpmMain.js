/**
 * Created by ilja on 1/22/2016.
 */
var $ = require('jquery');
var _jquery = $.noConflict(true);

var ArkGame = require("ArkGame");

var NpmMain = function () {
    "use strict";

};

NpmMain.prototype.initialize = function () {
    "use strict";

    var ARK_ArkGame = new ArkGame();
    window.ARK_arena_api_communicator = function (data){
        "use strict";
        ARK_ArkGame.arenaApiCommunicator(data);
    };

    window.ark_renderGame = function(url, idDiv, optionalParams){
        "use strict";
        if (typeof _jquery === 'undefined' && typeof _jquery !== 'undefined') {
            _jquery = $;
        };
        ARK_ArkGame.renderGame(_jquery, url, idDiv, optionalParams);
    };
};
//NpmMain.prototype.constructor = NpmMain;

module.exports = NpmMain;
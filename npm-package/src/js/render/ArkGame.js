/**
 * Created by ilja on 12/8/2015.
 */

var ParamsUtils = require("ParamsUtils");
var DeviceUtils = require("DeviceUtils");
var GamePreference = require("GamePreference");
var RendererInpage = require("./renderer/RendererInpage");
var RendererIframe = require("./renderer/RendererIframe");

var ArkGame = function () {
    "use strict";

    this._savedGlobalViewportMeta = undefined;
    this._gamePreferenceDropCache = 0;
    this._gamePreference = new GamePreference();
    this._iframeContentWindow = {};
    this._forceRenderType;
    this._url;
    this._idDiv;
    this._optionalParams;
    this._intervals = [];
};

ArkGame.prototype = {

    /*
    * Initial method
    * @param {object} jquery - jquery instance
    * @param {string} url - url string to load the game from
    * @param {string} idDiv - id of the div the game is rendering to
    * @param {object} optionalParams - object with optional params
    */
    renderGame: function (jquery, url, idDiv, optionalParams) {
        "use strict";
        this._jquery = jquery;
        this._url = url;
        this._idDiv = idDiv;
        this._optionalParams = optionalParams;
        this._forceRenderType = ParamsUtils.checkForceRenderType(optionalParams);

        if(this._jquery('#ARK_gamePreference').attr('src') != (url + '/preference.js?' + this._gamePreferenceDropCache)){
            this._gamePreferenceDropCache = Math.random().toFixed(5);
            this._jquery('#ARK_gamePreference').remove();

            this._jquery("#" + idDiv).html('');

           /* if(DeviceUtils.GetDeviceType() === DeviceUtils.DeviceType.pc){
                ARK_onresizeDivScreen = this._jquery("#" + idDiv)[0];
            }*/
            this._gamePreference.loadExternalPreference(this._jquery, url + '/preference.js?' + this._gamePreferenceDropCache, optionalParams, (this.onExternalPreferenceLoaded).bind(this))
        } else {
            /*if(typeof ARK_game_restartGame == 'function') {
                ARK_game_restartGame();
            } else */{
                this._jquery('#ARK_gamePreference').remove();
                this.renderGame(this._jquery, url, idDiv, optionalParams);
            }
        }

    },

    onExternalPreferenceLoaded: function () {
        "use strict";
        this.changeGlobalViewport();
        this.chooseRenderType();
    },

    /*
    * Chooses how the game should be rendered - in iframe or in "in-page"
    *
     */
    chooseRenderType:function (){
        "use strict";
        var renderType = (this._forceRenderType === 'in-page' || this._forceRenderType === 'iframe') ? this._forceRenderType : this._gamePreference.render;
        switch (renderType) {
            case 'in-page':
                this.renderGameIn_inpage(this._optionalParams, this._idDiv, this._url);
                break;
            case 'iframe':
                this.renderGameIn_iframe(this._optionalParams, this._idDiv, this._url);
                break;
            default:
                console.log('unknown render method');
                break;
        }
        //add listener destroying render timer after game-end for arena
        this.addListenerForGameDestroy();
        this.setCssDimetions(this._idDiv, this._optionalParams);
    },

    /*
    * Render the game in iframe
    *
     */
    renderGameIn_iframe: function (){
        "use strict";

        var rendererIframe = new RendererIframe();
        this._iframeContentWindow = rendererIframe.render(this._jquery, this._optionalParams, this._url, this._idDiv);

        var scaleInfo = {scale:'auto',idDiv:this._idDiv,hideNavBar:true};
        if(this._optionalParams != undefined && this._optionalParams.iframeScale != undefined){
            scaleInfo.scale = this._optionalParams.iframeScale;
        }
        if(this._optionalParams != undefined && this._optionalParams.hideNavBar != undefined){
            scaleInfo.hideNavBar = this._optionalParams.hideNavBar;
        }
        this.resize(scaleInfo);
    },

    /*
     * Render the game in-page
     *
     */
    renderGameIn_inpage:function (){
        "use strict";

        var rendererInPage = new RendererInpage();
        rendererInPage.render(this._jquery, this._optionalParams, this._url, this._idDiv);
    },

    /**
     * @return {boolean}
     */
    resizeHeightCalibration: function(){
        "use strict";
        return this._gamePreference.width != undefined && this._gamePreference.height != undefined && (DeviceUtils.GetDeviceType() == DeviceUtils.DeviceType.phone || DeviceUtils.GetDeviceType() == DeviceUtils.DeviceType.tablet);
    },

    /*
     * Resize the game container
     * @param {object} scaleInfo - object {scale:'auto',idDiv:this._idDiv,hideNavBar:true}
     *
     */
    resize:function (scaleInfo){
        "use strict";
        var resize_vars = {};
        resize_vars.scaleMethod = scaleInfo.scale;
        resize_vars.width = 0;
        resize_vars.height = 0;
        resize_vars.hideNavBar = scaleInfo.hideNavBar;
        resize_vars.pageYOffset = 0;

        if(resize_vars.scaleMethod == 'auto'){
            if(DeviceUtils.GetDeviceType() == DeviceUtils.DeviceType.phone || DeviceUtils.GetDeviceType() == DeviceUtils.DeviceType.tablet){
                resize_vars.scaleMethod = 'full';
            } else {
                resize_vars.scaleMethod = 'div';
            }
        }
        if(this.resizeHeightCalibration()){
            this._jquery("#" + scaleInfo.idDiv).css({
    //            'overflow':'auto',
                '-webkit-overflow-scrolling':'touch'
            });
        }
        var interval = setInterval ((function (){
            if(resize_vars.hideNavBar && resize_vars.pageYOffset != 0 ||
                resize_vars.hideNavBar && window.pageXOffset != 0){
                window.scrollTo(0, 0);
            }
            resize_vars.pageYOffset = window.pageYOffset;
            if(window.pageXOffset != 0){
                window.scrollTo(0, window.pageYOffset);
            }

            var width;
            var height;
            if(resize_vars.scaleMethod == 'div'){
                width = this._jquery("#" + scaleInfo.idDiv).width();
                height = this._jquery("#" + scaleInfo.idDiv).height();
            } else {
                if (DeviceUtils.GetDeviceType() === DeviceUtils.DeviceType.pc) {
                    width = this._jquery("#" + scaleInfo.idDiv).width();
                    height = this._jquery("#" + scaleInfo.idDiv).height();
                } else {
                    width = window.innerWidth;
                    height = window.innerHeight + resize_vars.pageYOffset;
                    // iPhone 6+ detect (bug in landscape mode)
                    if(height == 44 && navigator.userAgent.match(/(iPhone)/i) && screen.width == 414 && screen.height == 736 && window.devicePixelRatio == 3){
                        height = 370;
                    }
                    // iPhone 6+ detect
                }
            }
            if(this.resizeHeightCalibration()){
                height = Math.round(this._gamePreference.height * (width / this._gamePreference.width));
            }
            if(resize_vars.width == width && resize_vars.height == height){
                return;
            }
            resize_vars.width = width;
            resize_vars.height = height;
            this._jquery("#" + scaleInfo.idDiv + ' > iframe').attr({width:width,height:height});
        }).bind(this),500);
        this._intervals.push(interval);
    },

    /*
     * Method for arena code to communicate with the game
     *  @param {string} data
     */
    arenaApiCommunicator : function (data) {
        "use strict";
        if(this._iframeContentWindow === 'undefined'){
            console.log('ARK_renderGame_iframe.contentWindow=undefined:' + data);
            return;
        }
        console.log('arenaApiCommunicator:' + data);
        this._iframeContentWindow.postMessage(data, '*');
    },

    changeGlobalViewport: function () {
        "use strict";
        if(this._jquery('head > meta[name="viewport"]').length != 0){
            this._savedGlobalViewportMeta =  this._jquery('head > meta[name="viewport"]').attr('content');
            document.getElementsByName("viewport")[0].setAttribute("content", this._gamePreference.meta);
        } else {
            this._jquery('head').append('<meta name="viewport" content="' + this._gamePreference.meta + '">')
        }
    },

    restoreGlobalViewport:function (){
        "use strict";
        if(this._savedGlobalViewportMeta != undefined){
            document.getElementsByName("viewport")[0].setAttribute("content", '"'  + this._savedGlobalViewportMeta + '"' );
            window.scrollTo(0, 0);
        }
    },

    addListenerForGameDestroy:function (){
        "use strict";
        if (window.ark && window.ark.addListener){
            window.ark.addListener('arkGetGameEndEvent', (function() {
                /*if(typeof ARK_game_destroyGame == 'function'){
                    ARK_game_destroyGame();
                    ARK_game_destroyGame = undefined;
                }*/
                setTimeout((this.destroyGame).bind(this),10)
            }).bind(this));
        }
    },

    /*
    * Destroys the game container and restores the viewport
     */
    destroyGame:function (){
        "use strict";
        this.restoreGlobalViewport();
        this._jquery.each(this._intervals, function(i, val){
            clearInterval(val);
        });
    },

    setCssDimetions: function (idDiv, optionalParams){
        "use strict";
        if(
            DeviceUtils.GetDeviceType() === DeviceUtils.DeviceType.pc &&
            optionalParams !== undefined &&
            optionalParams.width !== undefined &&
            optionalParams.height !== undefined &&
            optionalParams.width !== 0 &&
            optionalParams.height !== 0
        ){
            this._jquery("#" + idDiv).css({width:parseInt(optionalParams.width),height:parseInt(optionalParams.height)})
        }
    }



};

module.exports = ArkGame;
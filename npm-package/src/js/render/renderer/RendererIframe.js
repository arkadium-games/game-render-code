/**
 * Created by ilja on 12/24/2015.
 */

var ParamsUtils = require("ParamsUtils");

var RendererIframe = function () {
    "use strict";

};

RendererIframe.prototype.render = function (jquery, optionalParams, url, idDiv) {
    "use strict";

    this._jquery = jquery;
    this._optionalParams = optionalParams;
    this._url = url;
    this._idDiv = idDiv;
    var gameProtocolParams = ParamsUtils.checkGameProtocolParams(this._optionalParams);
    var index = '/index.html';
    if(this._optionalParams != undefined && !this._optionalParams.correctURL){
        index = '';
    }
    this._jquery("#" + this._idDiv).html('' +
        '<iframe ' +
        'frameborder="0" ' +
        'width="' + this._jquery("#" + this._idDiv).width() + '" ' +
        'height="' + this._jquery("#" + this._idDiv).height() + '" ' +
        'src="' + this._url + index + gameProtocolParams + '" ' +
        'style="display:block" ' +
        'scrolling="no"' +
        '></iframe>');
    var iframeContentWindow = this._jquery("#" + this._idDiv + ' > iframe')[0].contentWindow;
    if ('arkPage' in window && window.arkPage.postMessage != null) {
        // we hosted at Arkadium Arena site
        if (window.addEventListener) {
            window.addEventListener('message', this.gameApiCommunicator, false);
        } else {
            window.attachEvent('onmessage', this.gameApiCommunicator);
        }
        window.arkPage.postMessage('event=game_loaded&callback=ARK_arena_api_communicator');
    }

    return iframeContentWindow;
};

/*
 * Method for the game to communicate with the arena
 *  @param {object} event
 */
RendererIframe.prototype.gameApiCommunicator = function (event) {
    "use strict";
    console.log('gameApiCommunicator: <' + event.data + '>');
    window.arkPage.postMessage(event.data);
},


module.exports = RendererIframe;
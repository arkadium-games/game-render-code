/**
 * Created by ilja on 11/18/2015.
 */

var $ = require('jquery');
var _jquery = $.noConflict(true);

var ArkGame = require("ArkGame");
var ARK_ArkGame = new ArkGame();

/*
* Method for arena code to communicate with the game
*  @param {string} data
 */
ARK_arena_api_communicator = function(data){
    "use strict";
    ARK_ArkGame.arenaApiCommunicator(data);
}

/*
*   Creates a container for rendering the game.
*
*   @param {string} url - url string to load the game from
*   @param {string} idDiv - id of the div the game is rendering to
*   @param {object} optionalParams - object with filed
*                                       gameProtocolParams - initParams, 'show_game_end=false&locale=en-US&device_type=pc&arena_name=Render+Code+Test+Arena&game_name=Mahjongg+Candy&events=game_start%2cgame_end%2cpause_ready%2csign_in'
*                                       iframeId - 'customGameIframe'
 */
ark_renderGame = function(url, idDiv, optionalParams){
    "use strict";
    if (typeof _jquery === 'undefined' && typeof _jquery !== 'undefined') {
        _jquery = $;
    };
    ARK_ArkGame.renderGame(_jquery, url, idDiv, optionalParams);
};

//Help for preference.js
//{
//    'render':'iframe/in-page',
//    'meta":'target-densitydpi=device-dpi, user-scalable=no, initial-scale=1, minimum-scale=1, maximum-scale=1',
//    'optionalParams':{hideNavBar:false,iframeScale:'div'}
//}
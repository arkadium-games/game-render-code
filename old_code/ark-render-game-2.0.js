    ///////////////////////////// ark-render-game START
    ///////////////////////////// ark-render-game v.2.0.1

    var ARK_game={
        intervals:[]
    };

    function ark_addListener(obj, type, fn) {
        if (obj.addEventListener) {
            obj.addEventListener(type, fn, false);
        } else { // this is IE
            obj.attachEvent('on' + type, fn);
        }
    }


    function ark_setGameMetaViewport() {
        var metaEl = document.querySelector('meta[name="viewport"]');
        if (!metaEl) {
            metaEl = document.createElement('meta');
             metaEl.setAttribute('name', 'viewport');
             metaEl.setAttribute('content', 'target-densitydpi=device-dpi,maximum-scale=1,minimum-scale=1,user-scalable=no,initial-scale=1,width=device-width');
            document.head.appendChild(metaEl); // TODO: ensure that "document.head" does exist
        } else {
        	metaEl.setAttribute('content', 'target-densitydpi=device-dpi,maximum-scale=1,minimum-scale=1,user-scalable=no,initial-scale=1,width=device-width');
		}
    }


    function ark_createGame(url, idDiv, optionalParams) {
        ark_setGameMetaViewport();

        var iframeId = null;
        var width = null;
        var height = null;
        var gameProtocolParams = '';

        if (optionalParams && typeof optionalParams === 'object') {
            if (optionalParams.width && typeof optionalParams.width === 'number') {
                width = optionalParams.width;
            }
            if (optionalParams.height && typeof optionalParams.height === 'number') {
                height = optionalParams.height;
            }
            if (optionalParams.gameProtocolParams && typeof optionalParams.gameProtocolParams === 'string' && optionalParams.gameProtocolParams.length > 0) {
                gameProtocolParams = '?' + optionalParams.gameProtocolParams;
            }
            if (optionalParams.iframeId) {
                iframeId = optionalParams.iframeId;
            }
        }

        var fullURL = url + 'index.html' + gameProtocolParams;

        var frame = document.createElement('iframe');
        if (iframeId) {
            frame.setAttribute('id', iframeId);
        }
        frame.setAttribute('src', fullURL);
        frame.setAttribute('frameborder','0');
        frame.setAttribute('scrolling','no');
        frame.onload = function() {
            frame.contentWindow.focus();
        };
        frame.style.width = (width == null ? window.innerWidth : width) + 'px';
        frame.style.height = (height == null ? window.innerHeight : height) + 'px';
        document.getElementById(idDiv).appendChild(frame);

        var ARK_iFrame_position = ''
        var ARK_naw_hide = true
        function onResize(info) {
            var landscapeOrientation = (window.innerWidth > window.innerHeight);
            setTimeout(function() {
//                if(landscapeOrientation && navigator.appCodeName.search('Mozilla') > -1
//                    && navigator.appVersion.search('OS 7_0') > -1
//                    && navigator.appVersion.search('iPad') > -1)
//				{ // AND IF IPAD iOS7
//                    frame.style.top = '0';
//                    frame.style.left = '0';
//                    frame.style.position = 'fixed';
////                    document.getElementById(idDiv).style.top = '10px';
////                    document.getElementById(idDiv).style.position = 'absolute';
//                }

                frame.style.top = '0';
                frame.style.left = '0';
                frame.style.position = 'fixed';
                frame.style.width = info.width;
                frame.style.height = info.height;
            }, 30);
        } // checkOrientationChange END

//        ark_addListener(window, 'resize', onResize);
        if(arkPage.arena.device != 'pc'){
            ARK_onResize({callback:onResize});
        }
    } // ark_createGame END

    function ark_renderGameDestroy(){
        ark_jQuery.each(ARK_game.intervals, function(i,val){
            clearInterval(val);
        });
    }

    var ARK_onResize_var = {width:0, height:0, scale:1, maxScale:1, hideNavBar: true}
    function ARK_onResize(info){
        if(info.maxScale != undefined){
            ARK_onResize_var.maxScale = info.maxScale;
        }
        if(info.hideNavBar != undefined){
            ARK_onResize_var.hideNavBar = info.hideNavBar;
        }
        var interval = setInterval (function (){
            var width = window.innerWidth,
                height = window.innerHeight + window.pageYOffset;
            if(ARK_onResize_var.width == width && ARK_onResize_var.height == height){
                return
            }
            ARK_onResize_var.width = width;
            ARK_onResize_var.height = height;
            if(ARK_onResize_var.hideNavBar){
                window.scrollTo(0, 0);
            }
            ARK_onResize_var.scale = ARK_onResize_var.maxScale;
            if(info.gameWidth != undefined && info.gameHeight != undefined){
                while(
                    (info.gameWidth * ARK_onResize_var.scale) > ARK_onResize_var.width ||
                        (info.gameHeight * ARK_onResize_var.scale) > ARK_onResize_var.height){
                    ARK_onResize_var.scale -= 0.001;
                }
            }
            ARK_onResize_var.scale = ARK_onResize_var.scale.toFixed(3);
            info.callback(ARK_onResize_var);
        },500);
        ARK_game.intervals.push(interval);
    }
    //function callbackFunction(info){
    //    console.log(info.width) // new screen width
    //    console.log(info.height) // new screen height
    //    console.log(info.scale) //  new scale rate (gameWidth & gameHeight to new screen size)
    //    console.log(info)
    //}
    //ARK_onResize({gameWidth:1024,gameHeight:671,maxScale:10,callback:callbackFunction,hideNavBar:false});



    ///////////////////////////// ark-render-game END
function ark_addListener(obj, type, fn) {
	if (obj.addEventListener) {
		obj.addEventListener(type, fn, false);
	} else { // this is IE
		obj.attachEvent('on' + type, fn);
	}
}


function ark_createGame(url, idDiv, width, height) {
	var frame = document.createElement('iframe');
    var viewport = document.querySelector("meta[name=viewport]");
    frame.setAttribute('src', url + 'index.html');
    frame.setAttribute('frameborder','0');
    frame.setAttribute('scrolling','no');

	var heightGame = frame.style.height = height == null ? window.innerHeight + 'px' : height + 'px';
	var widthGame = frame.style.width = width == null ? window.innerWidth + 'px' : width + 'px';

    document.getElementById(idDiv).appendChild(frame);

	function checkOrientationChange() {
    	switch (window.orientation) {
        	case -90:
        	case 90:
            	frame.style.height = heightGame;
            	frame.style.width = widthGame;
            	break;
        	default:
            	frame.style.height = window.innerHeight + 'px';
            	frame.style.width = window.innerWidth + 'px';
            	break;
    	}
	} // checkOrientationChange END

	ark_addListener(window, 'orientationchange', checkOrientationChange);
	window.setTimeout(checkOrientationChange, 500);
} // ark_createGame END